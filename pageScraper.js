// Define the goto function.
async function goto (page, url) {
    let retries = 30, retry = 0, timeout = [3000, 30000, 300000];
    while (retry < retries) {
      const response = await page.goto(url, {
        waitUntil: 'domcontentloaded',
        // Remove the timeout
        timeout: timeout[retry]
      });
      const text = await response.text()
      if (!text.includes('rejected')) {
        retry = 30;
      } else {
        await page.waitForTimeout(90000);
        // await page.waitForTimeout(timeout[retry]);
      }
      process.stdout.write(".");
      retry++;
    }
    console.log(url)
  }

const scraperObject = {
    url: 'https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW',
    async scraper(browser){

        const kws = [
            "WA2M/.../...",
        ];

        for (const k of kws) {
            let retry = true;
            while (retry) {
            try {

            const context = await browser.createIncognitoBrowserContext();
            const page = await context.newPage();

            console.log(`Navigating to ${this.url}...`);
            await goto(page, 'https://przegladarka-ekw.ms.gov.pl/eukw_prz/KsiegiWieczyste/wyszukiwanieKW');

            const kw = k.split('/')

            await page.waitForSelector('select[id=kodWydzialu]');
            await page.waitForSelector('input[name=numerKw]');
            await page.waitForSelector('input[name=cyfraKontrolna]');

            await page.$eval('select[id=kodWydzialu]', (el, value) => el.value = value, kw[0]);
            await page.$eval('input[name=numerKw]', (el, value) => el.value = value, kw[1]);
            await page.$eval('input[name=cyfraKontrolna]', (el, value) => el.value = value, kw[2]);

            await page.waitForSelector('button[id=wyszukaj]');
            await page.$eval('button[id=wyszukaj]', el => el.click());

            await page.waitForSelector('button[name=przyciskWydrukZwykly]');
            await page.click('button[name=przyciskWydrukZwykly]');

            await page.waitForSelector('input[value="Dział II"]');
            await page.click('input[value="Dział II"]');

            await page.waitForSelector('.csDane');
            const data = await page.$$eval('.csDane', tds => tds.map((td) => {
                return td.innerText;
            }));
            console.log(kw, data.filter((l) => l[0] != 'O' && l.split(',').length == 4));

            await context.close(); // clear history
            retry = false;
            } catch (err) {
                console.error(err);
                retry = true;
            }
        }
        }

        await browser.close();
    }
}

module.exports = scraperObject;
